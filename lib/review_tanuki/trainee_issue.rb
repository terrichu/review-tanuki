# frozen_string_literal: true

module ReviewTanuki
  class TraineeIssue
    attr_reader :iid, :project

    def initialize(client, project, iid)
      @client = client
      @project = project
      @iid = iid
    end

    def notes
      @client.issue_notes(project, iid)
    end

    def trainee
      @trainee ||= @client.user
    end

    def create_note(body)
      @client.create_issue_note(@project, @iid, body)
    end

    def update_note(note_id, body)
      @client.edit_issue_note(@project, @iid, note_id, body)
    end
  end
end
